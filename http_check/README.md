

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_monitoring_alert_policy.ssl_cert_expiry](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/monitoring_alert_policy) | resource |
| [google_monitoring_alert_policy.uptime_alert](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/monitoring_alert_policy) | resource |
| [google_monitoring_uptime_check_config.https](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/monitoring_uptime_check_config) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_alert_policy_filter"></a> [alert\_policy\_filter](#input\_alert\_policy\_filter) | n/a | `string` | `""` | no |
| <a name="input_duration"></a> [duration](#input\_duration) | n/a | `string` | `"120s"` | no |
| <a name="input_http_check_path"></a> [http\_check\_path](#input\_http\_check\_path) | n/a | `string` | `"/"` | no |
| <a name="input_http_check_port"></a> [http\_check\_port](#input\_http\_check\_port) | n/a | `string` | `"443"` | no |
| <a name="input_http_check_status_class"></a> [http\_check\_status\_class](#input\_http\_check\_status\_class) | n/a | `string` | `"STATUS_CLASS_2XX"` | no |
| <a name="input_http_check_use_ssl"></a> [http\_check\_use\_ssl](#input\_http\_check\_use\_ssl) | n/a | `bool` | `true` | no |
| <a name="input_http_check_validate_ssl"></a> [http\_check\_validate\_ssl](#input\_http\_check\_validate\_ssl) | n/a | `bool` | `true` | no |
| <a name="input_monitored_resource_host"></a> [monitored\_resource\_host](#input\_monitored\_resource\_host) | n/a | `string` | n/a | yes |
| <a name="input_monitoring_project"></a> [monitoring\_project](#input\_monitoring\_project) | n/a | `string` | `""` | no |
| <a name="input_name"></a> [name](#input\_name) | n/a | `string` | `"https-uptime-check"` | no |
| <a name="input_notification_channels"></a> [notification\_channels](#input\_notification\_channels) | n/a | `list(string)` | `[]` | no |
| <a name="input_period"></a> [period](#input\_period) | n/a | `string` | `"60s"` | no |
| <a name="input_project"></a> [project](#input\_project) | n/a | `string` | n/a | yes |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | n/a | `string` | `"30s"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->