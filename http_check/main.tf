locals {
  monitoring_project = var.monitoring_project == "" ? var.project : var.monitoring_project
}

resource "google_monitoring_uptime_check_config" "https" {
  project = local.monitoring_project

  display_name = var.name
  timeout      = var.timeout
  period       = var.period

  http_check {
    path         = var.http_check_path
    port         = var.http_check_port
    use_ssl      = var.http_check_use_ssl
    validate_ssl = var.http_check_validate_ssl
    accepted_response_status_codes {
      status_class = var.http_check_status_class
      status_value = 0
    }
  }

  monitored_resource {
    type = "uptime_url"
    labels = {
      project_id = var.project
      host       = var.monitored_resource_host
    }
  }

  checker_type = "STATIC_IP_CHECKERS"
}

resource "google_monitoring_alert_policy" "uptime_alert" {
  project               = local.monitoring_project
  display_name          = "${var.name} availability"
  notification_channels = var.notification_channels

  combiner = "OR"

  conditions {
    display_name = "${var.name} availability run"

    condition_absent {
      filter = trimspace(replace(
        <<-EOT
        metric.type="monitoring.googleapis.com/uptime_check/check_passed" AND
        metric.label.check_id="${google_monitoring_uptime_check_config.https.uptime_check_id}" AND
        resource.type="uptime_url"
        EOT
        , "\n", " "
      ))
      # absent conditions have to have a min duration of 2 minutes
      duration = var.duration

      aggregations {
        # absent conditions have to have a min duration of 2 minutes
        alignment_period     = "120s"
        cross_series_reducer = "REDUCE_COUNT"
        group_by_fields      = []
        per_series_aligner   = "ALIGN_COUNT"
      }

      trigger { count = 1 }
    }
  }

  conditions {
    display_name = "${var.name} availability passed"

    condition_threshold {
      filter = trimspace(replace(
        <<-EOT
        metric.type="monitoring.googleapis.com/uptime_check/check_passed" AND
        metric.label.check_id="${google_monitoring_uptime_check_config.https.uptime_check_id}" AND
        resource.type="uptime_url"
        EOT
        , "\n", " "
      ))
      duration   = "120s"
      comparison = "COMPARISON_LT"

      threshold_value = 0.9

      aggregations {
        alignment_period     = "120s"
        cross_series_reducer = "REDUCE_FRACTION_TRUE"
        per_series_aligner   = "ALIGN_NEXT_OLDER"
      }

      trigger { count = 1 }
    }
  }

  user_labels = {
    domain = replace(var.monitored_resource_host, ".", "-")
    action = "http_check"
  }
}

resource "google_monitoring_alert_policy" "ssl_cert_expiry" {
  project               = local.monitoring_project
  display_name          = "${var.name} ssl certificate"
  notification_channels = var.notification_channels

  combiner = "OR"
  conditions {
    display_name = "${var.name} ssl certificate"

    condition_threshold {
      filter = trimspace(replace(
        <<-EOT
        metric.type="monitoring.googleapis.com/uptime_check/time_until_ssl_cert_expires" AND
        metric.label.check_id="${google_monitoring_uptime_check_config.https.uptime_check_id}" AND
        resource.type="uptime_url"
        EOT
        , "\n", " "
      ))
      duration   = var.duration
      comparison = "COMPARISON_LT"

      threshold_value = 14

      trigger { count = 1 }
    }
  }

  user_labels = {
    domain = replace(var.monitored_resource_host, ".", "-")
    action = "ssl_check"
  }

}

