variable "name" {
  type    = string
  default = "https-uptime-check"
}

variable "project" {
  type = string
}

variable "monitoring_project" {
  type    = string
  default = ""
}

variable "timeout" {
  type    = string
  default = "30s"
}

variable "period" {
  type    = string
  default = "60s"
}

variable "duration" {
  type    = string
  default = "120s"
}

variable "http_check_path" {
  type    = string
  default = "/"
}

variable "http_check_port" {
  type    = string
  default = "443"
}

variable "http_check_use_ssl" {
  type    = bool
  default = true
}

variable "http_check_validate_ssl" {
  type    = bool
  default = true
}

variable "http_check_status_class" {
  type    = string
  default = "STATUS_CLASS_2XX"
}

variable "monitored_resource_host" {
  type = string
}

variable "alert_policy_filter" {
  type    = string
  default = ""
}

variable "notification_channels" {
  type    = list(string)
  default = []
}
